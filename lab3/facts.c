// Jeremy Wenzel
// id = jhw866
// section number = 
// latest update: 10/16/13
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/* Set Boolean Variable if needed */
typedef short Boolean;
#define TRUE 1
#define FALSE 0

Boolean questionsFound = FALSE;
Boolean factFound = FALSE; // Used to know if Fact file has been found
Boolean newLineHit = FALSE;


void outOfMemory(void) {
	printf("We are out of memory");
	exit(-1);
}

/******************************************************************/
/*								  */
/*		    Linked List Data Structure			  */
/*								  */
/******************************************************************/

/* Linked List Data Structure */
struct linked_list {
	char *answer;
	char *system;
	char *question;
	struct linked_list *next;
};

// keep track of beginning and current nodes
struct linked_list* header = NULL;
struct linked_list* current = NULL;

// The list to hold all the facts
struct linked_list *facts;
// mostly for debugging purposes
int numNodes = 0;

struct linked_list *makeList(char * value, char *system, char *question) {
	// allocate memory for node
	struct linked_list *ptr = malloc(sizeof(struct linked_list));
	// if we are out of memory, exit
	if(ptr == NULL) {
		printf("Ran out of memory :(. Sorry but have to exit");
		outOfMemory();
	}

	// start setting values
	ptr->answer = value;
	ptr->system = system;
	ptr->question = question;
	//printf("Answer is: %i\n", ptr->answer);
	header = current = ptr;
	numNodes++;
	return ptr;
}


struct linked_list *searchList(char *system, char *question) {
	int check;
	// there are no nodes in the list	
	if(header == NULL)
		return NULL;
	// start at header node
	struct linked_list *ptr = header;
	while(ptr != NULL) {
		check = strcmp(system, ptr->system);
		// if the systems are the same
		if(check == 0) {
			check = strcmp(question, ptr->question);
			// if the questions are the same
			if(check == 0) 
				return ptr;
		}
		// get next item in linked_list
		ptr = ptr->next;
	}
	return NULL;
}

void addToList(char *value, char *system, char *question) {
	// if we haven't even made the node, create one
	if(header == NULL) {
		makeList(value, system, question);
		return;
	}

	// check if in Linked_List already
	struct linked_list *ptr = searchList(system, question);
	if(ptr != NULL) {
		ptr-> answer = value;
		return;
	}

	// error cases checked
	// begin to actually adding to the list
	ptr= malloc(sizeof(struct linked_list));
	if(ptr == NULL) {
		printf("Ran out of memory :(. Sorry but have to exit");
		return;
	}

	// start setting values
	ptr->answer = value;
	ptr->system = system;
	ptr->question = question;
	current->next = ptr;
	current = ptr;
	numNodes++;
	
}
/******************************************************************/
/*								  */
/*			   Read File				  */
/*								  */
/******************************************************************/
char* readWord(FILE *input, char delimiter) {
	int bufferLength = 0;
	int n = 0;
	char *buffer = NULL;
	char c = getc(input); 
	while((c == ' ') || (c == '\t'))
		c = getc(input);
	while((c != EOF) && (c != delimiter) && (c != '\n')) {
		// makes sure room for character and terminating byte
		if (n >= bufferLength - 1) {
			if(bufferLength == 0)
				bufferLength = 256;
			else
				bufferLength = bufferLength*2;
			buffer = realloc(buffer, bufferLength);
			if(buffer == NULL) {
				printf("Buffer is NULL in readFile");
				exit(-1); 
			}
		}
		buffer[n] = c;
		n++;
		c = getc(input);
	}
	if(c == '\n')
		newLineHit = TRUE;
	buffer[n] = '\0';
	if(n == 0)
		return(NULL);
	return buffer;
}


void readInput(FILE *input) {
	struct linked_list *finder;
	questionsFound = TRUE;
	Boolean line_formatted = TRUE;
	char *system;
	char *question;
	char *answer;
	char *ptr;
	char c = getc(input);
	while(c != EOF) {
		// making sure first character is an F or Q
		if(c != 'Q' || newLineHit == TRUE)
			line_formatted = FALSE;
		// read for system
		if(line_formatted == TRUE) { 
			system = readWord(input, ':');
			if(system == NULL)
				line_formatted = FALSE;
			
		}
		// read for question
		if(line_formatted == TRUE && newLineHit == FALSE) {
			question = readWord(input, '=');
			if(question == NULL) {
				line_formatted = FALSE;
				printf("This should be false");
			}
		}
		else
			line_formatted = FALSE;
		// get linked_list with correct info
		if(line_formatted == TRUE) {
			finder = searchList(system, question);
			if(finder == NULL)
				answer = "unknown";
			else
				answer = finder->answer;
			// lets start printing stuff
			printf("F ");
			ptr = &system[0];
			for(;*ptr != '\0'; ptr++)
				printf("%c", *ptr);
			printf(": ");
			ptr = &question[0];
			for(;*ptr != '\0'; ptr++)
				printf("%c", *ptr);
			printf("=");
			for(;*answer != '\0'; answer++)
				printf("%c", *answer);
			printf("\n");
			
			

		}
		// if line is not formatted correctly and newLine has not been hit
		if(line_formatted != TRUE && newLineHit == FALSE) {
			while(c != '\n')
				c = getc(input);
		}

		if(system != NULL) {
			free(system);
		}
		if(question != NULL)
			free(question);
		// reset stuff
		newLineHit = FALSE;
		answer = NULL;
		line_formatted = TRUE;
		c = getc(input);
	}
	
}

void readFacts(FILE *input) {
	Boolean line_formatted = TRUE;
	char *system;
	char *question;
	char *answer;
	char c = getc(input);
	while(c != EOF) {
		// making sure first character is an F or Q
		if(c != 'F' || newLineHit == TRUE)
			line_formatted = FALSE;
		// read for system
		if(line_formatted == TRUE) { 
			system = readWord(input, ':');
			if(system == NULL)
				line_formatted = FALSE;
		}
		// read for question
		if(line_formatted == TRUE && newLineHit == FALSE) {
			question = readWord(input, '=');
			if(question == NULL) {
				line_formatted = FALSE;
			}
		}
		// read for value
		if(line_formatted == TRUE && newLineHit == FALSE) {
			answer = readWord(input, '\n');
			if(answer == NULL)
				line_formatted = FALSE;
		}
		else
			line_formatted = FALSE;
		// put in linkedList
		if(line_formatted == TRUE){
			factFound = TRUE;
			addToList(answer, system, question);
		}
		// if line is not formatted correctly and newLine has not been hit
		if(line_formatted != TRUE && newLineHit == FALSE) {
			while(c != '\n')
				c = getc(input);
		}
		
		// reset stuff
		newLineHit = FALSE;
		answer = question = system = NULL;
		line_formatted = TRUE;
		c = getc(input);
	}

		
}
/******************************************************************/
/*								  */
/*			   Main Driver				  */
/*								  */
/******************************************************************/


int main(int argc, char *argv[]) {
	char *read; 
	int fact = 0;
	FILE *input;
	while(argc > 1) {
		argc--, argv++;
		read = *argv;
		// there are flags, but we really don't need them
		if(*read == '-')
			printf("No need for flags");
		// we are going to read the file now
		else {
			input = fopen(read, "r");
			if(input == NULL) {
				printf("could not open file");
				exit(-1);
			}
			else {
				// read for the first pass
				if(factFound == FALSE && fact == 0) {
					readFacts(input);
					fact = 1;		
				}
				// read for fact but didn't find it
				else if(fact == 1 && factFound == FALSE) {
					printf("Error, did not put in fax file\n");
					exit(-1);
				}
				else
					readInput(input);
				fclose(input);
			}
		}
	}

	if(questionsFound == FALSE && factFound == TRUE) {
		input = stdin;
		readInput(input);
	}
		
	if(factFound == FALSE)
		printf("did not provide any files");
	
	return 0;
}

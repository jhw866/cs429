#include "global.h"

// This file deals with the Register Memeory Reference Instructions
// in the pdp429 program
int reg_mask = (3 << 10);
Boolean jump;
// For non-Register Memory Reference Instructions
void nonRegMem(int opcode, int reg, int value, int address) {
	if(debug)
		fprintf(stdout, "In nonRegMem\n");
	if(opcode == 11) {
		// ISZ
		if(reg == 0) {
			if(debug)
				fprintf(stdout, "ISZ\n");
			value++;
			value = value & 65535;
			memory[address] = value;
			if(value == 0)
				skip = TRUE;
			time = time + 3;
		}

		// JMP
		if(reg == 1) {
			if(debug)
				fprintf(stdout, "JMP\n");
			regs[4] = address;
			jump = TRUE;
			time = time + 1;
		}

		// CALL
		if(reg == 2) {
					
			memory[regs[6]] = regs[4];	// pc + 1 goes to stack
			regs[6]--;
			regs[4] = address;
			jump = TRUE;
			time = time + 2;
		}
	}

	if(opcode == 12) {
		time = time + 2;
		// PUSH
		if(reg == 0) {
			if(debug)
				fprintf(stdout, "PUSH\n");
			if(regs[6] < regs[7]) {
				fprintf(stderr, "Stack Pointer = 0x%04X; Stack Limit = 0x%04X\n", regs[6], regs[7]);
				overflow = TRUE;
				halt = TRUE;
				return;
			}
			memory[regs[6]] = value;
			regs[6]--;
			time++;
		}

		/// POP
		if(reg == 1) {
			if(debug)
				fprintf(stdout, "POP\n");
			if(regs[6] == 65535) {
				fprintf(stderr, "Stack Pointer = 0x%04X; Pop = 0x0000: Underflow", regs[6]);
				overflow = TRUE;
				halt = TRUE;
				return;
			}
			regs[6]++;
			value = memory[regs[6]];
			memory[address] = value;
			time++;
		}
	}
}

// For Register memory Reference Instructions
void regMemInst(int opcode, int reg, int value, int address) {

	value3 = regs[reg];
	if(debug)
		fprintf(stdout, "In regMemInst\n");
	time = time + 2;
	short j = regs[reg];
	short k = value;
	short i;
	// ADD (Register + Memory-Operand) into register
	if(opcode == 1) {
		if(debug)
			fprintf(stdout, "ADD value 0x%04X and 0x%04X", regs[reg], value);
		i = j + k;
		if(j > 0 && k > 0 && i < 0) {
			overflow = TRUE;
			link = 1;
		}
		if(j < 0 && k < 0 && i > 0) {
			overflow = TRUE;
			link = 1;
		}
		value = i;
		//value = regs[reg] + value;
		regs[reg] = (value & 65535);
		
	}

	// SUB
	if(opcode == 2) {
		if(debug)
			fprintf(stdout, "SUB value 0x%04X and 0x%04X", regs[reg], value);
		i = j - k;
		if(j < 0 && k > 0 && i > 0) {
			overflow = TRUE;
			link = 1;
		}
		value = i;
		regs[reg] = (value & 65535);
	}

	// MUL
	if(opcode == 3) {
		if(debug)
			fprintf(stdout, "MUL value 0x%04X and 0x%04X", regs[reg], value);

		//check_link = (regs[reg] >> 15) & 1;
		i = regs[reg] * value;
		//check_over_flow = (value >> 15) & 1;
		if(regs[reg] > 0 && value > 0 && i < 0) {
			overflow = TRUE;
			link = 1;
		}
		regs[reg] = (i & 65535);
	}

	// DIV
	if(opcode == 4) {
		if(debug)
			fprintf(stdout, "DIV value 0x%04X and 0x%04X", regs[reg], value);

		if(value != 0)
			value = regs[reg] / value;
		else {
			regs[reg] = 0;
			link = 1;
			overflow = TRUE;
		}
		regs[reg] = (value & 65535);
	}

	// AND
	if(opcode == 5) {
		if(debug)
			fprintf(stdout, "AND value 0x%04X and 0x%04X", regs[reg], value);

		value = regs[reg] & value;
		regs[reg] = (value & 65535);
	}

	// OR
	if(opcode == 6) {
		if(debug)
			fprintf(stdout, "OR value 0x%04X and 0x%04X", regs[reg], value);

		value = regs[reg] | value;
		regs[reg] = (value & 65535);
	}

	// XOR
	if(opcode == 7) {
		if(debug)
			fprintf(stdout, "XOR value 0x%04X and 0x%04X", regs[reg], value);

		value = regs[reg] ^ value;
		regs[reg] = (value & 65535);
	}

	// LD (Just put the memory operand into the register)
	if(opcode == 8) {
		if(debug)
			fprintf(stdout, "LD value 0x%04X and 0x%04X", regs[reg], value);

		regs[reg] = value;
	}

	// ST (Just put the register into the memory-operand
	if(opcode == 9) {
		if(debug)
			fprintf(stdout, "ST value 0x%04X and 0x%04X", regs[reg], value);

		value = regs[reg];
		memory[address] = value;
	}
	//fprintf(stdout, " for register %i\n", reg);
}

void reg_mem_inst(int opcode, unsigned short old_pc) {
	if(debug)
		fprintf(stdout, "In register memory instruction. ");
	int address;
	int value;
	jump = FALSE;
	indirect = FALSE;
	// get register
	int reg = ((memory[old_pc] & reg_mask) >> 10);
	reg1 = reg;

	// get d_i bit
	int d_i = ((memory[old_pc] >> 9) & 1);
	// get z_c bit
	int z_c = ((memory[old_pc] >> 8) & 1);

	// check if z_c bit 0/1
	if(z_c == 0)
		address = (memory[old_pc] & 255);
	else {
		z_c = ((255 << 8) & old_pc);
		address = z_c + (memory[old_pc] & 255);
	}

	
	// check if d_i bit 0/1
	if(d_i == 0)
		value = memory[address];
	else {
		address2 = address;
		address = memory[address];
		value2 = address;
		value = memory[address];
		indirect = TRUE;
		time++;
	}
	
	address1 = address;
	value1 = memory[address];
	//fprintf(stdout, "Reg = 0x%04X	Address = 0x%04X   Value = 0x%04X\n",regs[reg], address, value);
	
	////////////////* begin operations *////////////
	if((opcode >= 1) && (opcode <= 9))
		regMemInst(opcode, reg, value, address);
	else
		nonRegMem(opcode, reg, value, address); 

}

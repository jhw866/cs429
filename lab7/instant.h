#include "global.h"


extern void readFile(FILE *input);

extern void execute(void);

extern void reg_mem_inst(int opcode, unsigned short old_pc);

extern void do_io(int opcode, unsigned short old_pc);

extern void do_regToReg(unsigned short old_pc);

extern void do_nonMemReg(unsigned short old_pc);

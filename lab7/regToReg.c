#include "global.h"

/* Deals with the Register-to-Register Instructions for the pdp429 Program */

// sub opcode 7 << 9
// reg i 7 << 6
// reg j 7 << 3
// reg k 7

void do_regToReg(unsigned short old_pc) {
	int sub_opcode = (memory[old_pc] >> 9) & 7;
	int reg_i = (memory[old_pc] >> 6) & 7;
	reg1 = reg_i;
	int reg_j = (memory[old_pc] >> 3) & 7;
	reg2 = reg_j;
	int reg_k = (memory[old_pc]) & 7;
	reg3 = reg_k;

	short val;
	short j = regs[reg_j];
	short k = regs[reg_k];
	/* begin operations */
	//fprintf(stdout, "reg%i = 0x%04X reg%i = 0x%04X\n", reg_k, regs[reg_k], reg_j, regs[reg_j]);
	// MOD
	if(sub_opcode == 0) {
		if(regs[reg_k] != 0)
			val =j % k;
		else {
			overflow = TRUE;
			val = 0;
			link = 1;
		}
	}
	
	value2 = regs[reg_j];
	value3 = regs[reg_k];
	// ADD
	if(sub_opcode == 1) {
		val =j + k;
		if(j > 0 && k >0 && val < 0) {
			overflow = TRUE;
			link = 1;
		}
		if(j < 0 && k < 0 && val > 0) {
			overflow = TRUE;
			link = 1;
		}
	}
	
	// SUB
	if(sub_opcode == 2) {
		val =j - k;
		// if we have a (-x) - (+y) = (+z) 
		if(j < 0 && k > 0 && val > 0)
			overflow = TRUE;
	}

	// MUL
	if(sub_opcode == 3) {
		val =j * k;
		if(j > 0 && k > 0 && val < 0)
			overflow = TRUE;
	}

	// DIV
	if(sub_opcode == 4) {
		if(regs[reg_k] != 0)
			val =j / k;
		else {
			val = 0;
			link = 1;
			overflow = TRUE;
		}
	}

	// AND
	if(sub_opcode == 5) {
		val =j & k;
	}

	// OR
	if(sub_opcode == 6) {
		val =j | k;
	}

	// XOR
	if(sub_opcode == 7) {
		val =j ^ k;
	}

	// Make sure the PC does not over flow
	if(reg_i == 4) {
		val = val % 65535;
		overflow = FALSE;
	}
	regs[reg_i] = val;
	value1 = regs[reg_i];
	//fprintf(stdout, "reg%i is reg%i ___ reg%i = 0x%04X\n", reg_i, reg_j, reg_k, regs[reg_i]);
	time++;
}

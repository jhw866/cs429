#ifndef _GLOBAL_H_
#define _GLOBAL_H_
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Contains all the necessary C information and operators */
typedef short Boolean;
#define TRUE 1
#define FALSE 0

unsigned short memory[65536];
short mode;
short ep;		// Entry-Point
short pc;		// Program-Counter
Boolean debug;		// REMEMBER TO SWITCH TO FALSE!!!!!!!!
Boolean halt;
Boolean indirect;
Boolean skip;
Boolean overflow;
int time;
int increment;

unsigned short address1;
unsigned short address2;
unsigned short value1;
unsigned short value2;
unsigned short value3;
unsigned short reg1;
unsigned short reg2;
unsigned short reg3;
unsigned short compLink;
unsigned short skipLink;
// Registers
// A=0; B=1; C=2; D=3; PC=4; PSW=5; SP=6; SPL=7
unsigned short regs[8];
short link;
#endif

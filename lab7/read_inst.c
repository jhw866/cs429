#include "global.h"

short readSize(FILE *input) {
	char c = getc(input);
	unsigned short answer = 0;
	answer = answer + c;
	answer = answer & 255;
	if(feof(input)) 
		return -1;
	else
		return answer;
		
}

short readValues(FILE *input) {
	char c = getc(input);
	short answer;
	short answer2;
	if(feof(input)) {
		fprintf(stderr, "Value read is EOF. Exiting");
		exit(-1);
	}
	answer = c;
	answer = (answer << 8);
	c = getc(input);
	if(feof(input)) {
		fprintf(stderr, "Value read is EOF. Exiting");
		exit(-1);
	}
	answer2 = c;
	answer2 = answer2 & 255;
	answer = answer + answer2;
	return answer;
}

// initialize memory to 0
void intial(void) {
	int i = 0;
	increment = 0;
	while(i < 65536) {
		memory[i] = 0;
		i++;
	}
	i=0;
	while(i < 8) {
		regs[i] = 0;
		i++;
	}
	regs[5] = 1;
	link = 0;
	time = 0;
}
// main driver
void readFile(FILE *input) {
	char objg[5] = {0};
	int check = 0;
	short size;
	unsigned short address;
	unsigned short value;
	objg[0] = getc(input);
	objg[1] = getc(input);
	objg[2] = getc(input);
	objg[3] = getc(input);
	objg[4] = '\0';
	
	check = strcmp(objg, "OBJG");
	if(check != 0) {
		fprintf(stderr, "Input File does not contain 'OBJG'. Exiting");
		exit(-1);
	}	

	// get entry point
	ep = readValues(input);
	if(debug)
		fprintf(stderr, "EP: 0x%04x\n", ep);

	// begin getting blocks
	size = readSize(input);
	// intialize memory
	intial();
	while(size != -1) {
		if(debug)
			fprintf(stdout, "Size of block: 0x%04X\n", size);
		address = readValues(input);
		size = size - 3;
		while(size > 0) {
			value = readValues(input);
			if(debug)
				fprintf(stdout, "Memory address 0x%04X has value 0x%04X\n", address, value);
			memory[address] = value;
			address++;
			size = size - 2;
		}
		size = readSize(input);
	}
	if(debug)
		fprintf(stdout, "Finished Reading File\n");
}

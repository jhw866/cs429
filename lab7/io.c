#include "global.h"

// Does I/O Transfer Instructions for the PDP-429 Program

void do_io(int opcode, unsigned short old_pc) {
	char value;
	int reg = (memory[old_pc] >> 10) & 3;
	reg1 = reg;
	int device = (memory[old_pc] >> 3) & 127;
	if(device == 3) {
		value = getc(stdin);
		regs[reg] = value;
		regs[reg] = regs[reg] & 65535;
	}
	
	else if(device == 4) {
		value = (regs[reg] & 255);
		fprintf(stdout, "%c", value);
	}
	else {
		fprintf(stderr, "Error in I/O device. Exiting");
		exit(-1);
	}
	value1 = value;
	time++;
}

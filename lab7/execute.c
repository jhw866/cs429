#include "global.h"
#include "instant.h"

long long total_time = 0;
const int opcode_mask = (15 << 12);
// opcode mask = 15 << 12

void checkPSW(void) {
	if((regs[5] & 1) == 0)
		halt = TRUE;
}

void addReg(unsigned short inst, char names[]) {
		int check_reg = (inst >> 10) & 3;
		if(check_reg == 0) 
			strcat(names, "A");
		else if(check_reg == 1)
			strcat(names, "B");
		else if(check_reg == 2)
			strcat(names, "C");
		else
			strcat(names, "D");
}
void print_reg(unsigned short reg) {
	if(reg == 0)
		fprintf(stderr, "A");
	if(reg == 1)
		fprintf(stderr, "B");
	if(reg == 2)
		fprintf(stderr, "C");
	if(reg == 3)
		fprintf(stderr, "D");
	if(reg == 4)
		fprintf(stderr, "PC");
	if(reg == 5)
		fprintf(stderr, "PSW");
	if(reg == 6)
		fprintf(stderr, "SP");
	if(reg == 7)
		fprintf(stderr, "SPL");
}
void get_regs(int opcode, unsigned short inst) {
	
	int check_reg;
	fprintf(stderr, ": ");
	// need to know opcode
	if(opcode == 0) {
		if(inst == 0)
			return;
		if(inst == 1) {
			fprintf(stderr, "PSW -> 0x%04X, ", value1);
			fprintf(stderr, "0x%04X -> PSW", regs[5]);
		}
		if(inst == 2) {
			fprintf(stderr, "SP -> 0x%04X, ", value1);
			fprintf(stderr, "0x%04X -> SP, ", regs[6]);
			fprintf(stderr, "M[0x%04X] -> 0x%04X, ", regs[6], memory[regs[6]]);
			fprintf(stderr, "0x%04X -> PC", memory[regs[6]]);
		}
	}
	
	if(opcode >= 1 && opcode <= 7) {
		// if indirect do memory with address1
		if(indirect)
			fprintf(stderr, "M[0x%04X] -> 0x%04X, ", address2, value2);
		print_reg(reg1);
		fprintf(stderr, " -> 0x%04X, ", value3);
		fprintf(stderr, "M[0x%04X] -> 0x%04X, ", address1, value1);
		if(overflow)
			fprintf(stderr, "0x0001 -> L, ");
		fprintf(stderr, "0x%04X -> ", regs[reg1]);
		print_reg(reg1);
	}
	
	// LD
	if(opcode == 8) {
		if(indirect)
			fprintf(stderr, "M[0x%04X] -> 0x%04X, ", address2, value2);
		fprintf(stderr, "M[0x%04X] -> 0x%04X, ", address1, value1);
		fprintf(stderr, "0x%04X -> ", regs[reg1]);
		print_reg(reg1);
	}
	
	if(opcode == 9) {
		if(indirect)
			fprintf(stderr, "M[0x%04X] -> 0x%04X, ", address2, value2);

		print_reg(reg1);
		fprintf(stderr, " -> 0x%04X, ", regs[reg1]);
		fprintf(stderr, "0x%04X -> M[0x%04X]", regs[reg1], address1);
	}

	// IOT
	if(opcode == 10) {
		
		check_reg = (inst >> 3) & 127;
		if(check_reg == 3) {
			fprintf(stderr, "0x%04X -> ", value1);
			print_reg(reg1);
		}		

		else if(check_reg == 4) {
			print_reg(reg1);
			fprintf(stderr, " -> 0x%04X", value1);
		}
	
		else;
	}

	if(opcode == 11) {
		check_reg = (inst >> 10) & 3;
		if(indirect)
			fprintf(stderr, "M[0x%04X] -> 0x%04X, ", address2, value2);

		// ISZ
		if(check_reg == 0) {
			fprintf(stderr, "M[0x%04X] -> 0x%04X, ", address1, value1);
			value1 = value1 + 1;
			fprintf(stderr, "0x%04X -> M[0x%04X]", value1, address1);
		}

		// JMP
		if(check_reg == 1) {
			fprintf(stderr, "0x%04X -> PC", address1);
		}

		// CALL
		if(check_reg == 2) {
			fprintf(stderr, "0x%04X -> M[0x%04X], ", memory[regs[6] + 1], (regs[6]+1));
			fprintf(stderr, "0x%04X -> SP, ", regs[6]);
			fprintf(stderr, "0x%04X -> PC", regs[4]);
		}
	}

	if(opcode == 12) {
		check_reg = (inst >> 10) & 3;

		if(indirect)
			fprintf(stderr, "M[0x%04X] -> 0x%04X, ", address2, value2);
		if(overflow) {
			fprintf(stderr, "M[0x%04X] -> 0x%04X, ", address1, value1);
			fprintf(stderr, "PSW -> 0x%04X, ", regs[5]);
			regs[5] = (regs[5] ^ 1);
			fprintf(stderr, "0x%04X -> PSW\n", regs[5]);
			exit(-1);
		}
	
		// Push
		if(check_reg == 0) {
			fprintf(stderr, "M[0x%04X] -> 0x%04X, ", address1, value1);
			fprintf(stderr, "0x%04X -> M[0x%04X], ", value1, (regs[6]+1));
			fprintf(stderr, "0x%04X -> SP", regs[6]);
		}
		// Pop
		if(check_reg == 1) {
			fprintf(stderr, "SP -> 0x%04X, ", (regs[6]-1));
			fprintf(stderr, "0x%04X -> SP, ", regs[6]);
			fprintf(stderr, "M[0x%04X] -> 0x%04X, ", regs[6], memory[regs[6]]);
			fprintf(stderr, "0x%04X -> M[0x%04X]", memory[regs[6]], address1);
		}

	}
	
	if(opcode == 14) {
		// Second Reg
		print_reg(reg2);
		fprintf(stderr, " -> 0x%04X, ", value2);
		// Third Reg
		print_reg(reg3);
		fprintf(stderr, " -> 0x%04X, ", value3);
		if(overflow)
			fprintf(stderr, "0x0001 -> L, ");
		// Value into first Reg
		fprintf(stderr, "0x%04X -> ", value1);
		print_reg(reg1);
	}

	if(opcode == 15) {
		Boolean skipped = FALSE;
		Boolean started = FALSE;
		int check_bit = (inst >> 9) & 1;
		// sm
		if(check_bit == 1) {
			started = TRUE;
			print_reg(reg1);
			fprintf(stderr, " -> 0x%04X", value1);
			check_bit = (value1 >> 15) & 1;
			if(check_bit == 1)
				skipped = TRUE;
		}

		check_bit = (inst >> 8) & 1;
		// sz
		if(check_bit == 1) {
			if(!skipped) {
				if(started)
					fprintf(stderr, ", ");
				print_reg(reg1);
				fprintf(stderr, " -> 0x%04X", value1);
				started = TRUE;
				if(value1 == 0)
					skipped = TRUE;
				started = TRUE;
			}
		}

		// CLZ
		check_bit = (inst >> 7) & 1;
		if(check_bit == 1) {
			if(started)
				fprintf(stderr, ", ");
			fprintf(stderr, "L -> 0x%04X", skipLink);
			started = TRUE;
		}

		// RSS
		check_bit = (inst >> 6) & 1;
		if(check_bit == 1) {
			// not sure if we need this
			value1 = value1;
		}
	
		// CL
		check_bit = (inst >> 5) & 1;
		if(check_bit == 1) {
			if(started)
				fprintf(stderr, ", ");
			fprintf(stderr, "0x0000 -> ");
			print_reg(reg1);
			started = TRUE;
		}

		// CLL
		check_bit = (inst >> 4) & 1;
		if(check_bit == 1) {
			if(started)
				fprintf(stderr, ", ");
			fprintf(stderr, "0x0000 -> L");
			started = TRUE;
		}

		// CM
		check_bit = (inst >> 3) & 1;
		if(check_bit == 1) {
			if(started)
				fprintf(stderr, ", ");
			print_reg(reg1);
			fprintf(stderr, " -> 0x%04X, ", value2);
			value2 = ~value2;
			fprintf(stderr, "0x%04X -> ", value2);
			print_reg(reg1);
			started = TRUE;
		}

		// CML
		check_bit = (inst >> 2) & 1;
		if(check_bit == 1) {
			if(started)
				fprintf(stderr, ", ");
			fprintf(stderr, "L -> 0x%04X, ", compLink);
			compLink = (~compLink & 1);
			fprintf(stderr, "0x%04X -> L", compLink);
			started = TRUE;
		}
		
		// DEC
		check_bit = (inst >> 1) & 1;
		if(check_bit == 1) {
			if(started)
				fprintf(stderr, ", ");
			print_reg(reg1);
			fprintf(stderr, " -> 0x%04X, ", value3);
			if(overflow)
				fprintf(stderr, "0x0001 -> L, ");
			value3 = value3 -1;
			fprintf(stderr, "0x%04X -> ", value3);
			print_reg(reg1);
			started = TRUE;
		}

		check_bit = inst & 1;
		if(check_bit == 1) {
			if(started)
				fprintf(stderr, ", ");
			print_reg(reg1);
			fprintf(stderr, " -> 0x%04X, ", (regs[reg1]-1));
			if(overflow)
				fprintf(stderr, "0x0001 -> L, ");
			fprintf(stderr, "0x%04X -> ", (regs[reg1]));
			print_reg(reg1);
		}

	}
	if(skip)
		fprintf(stderr, ", 0x%04X -> PC", (regs[4] + 1));
	overflow = FALSE;



}
char *get_opcode_names(int opcode, unsigned short inst) {
	int check_reg = 0;
	int size = 0;
	char names[30] = {0};
	char* ret;
	Boolean place;
	if(opcode == 0) {
		if(inst == 0)
			strcat(names, "NOP");
		if(inst == 1)
			strcat(names, "HLT");
		if(inst == 2)
			strcat(names, "RET");
	}
	if(indirect)
			strcat(names, "I ");
	if(opcode == 1) {
		strcat(names, "ADD");
	}

	if(opcode == 2) {
		strcat(names, "SUB");
	}

	if(opcode == 3) {
		strcat(names, "MUL");
	}

	if(opcode == 4) {
		strcat(names, "DIV");
	}

	if(opcode == 5) {
		strcat(names, "AND");
	}

	if(opcode == 6) {
		strcat(names, "OR");
	}

	if(opcode == 7) {
		strcat(names, "XOR");
	}

	if(opcode == 8) {
		strcat(names, "LD");
	}

	if(opcode == 9) {
		strcat(names, "ST");
	}

	if(opcode == 10) {
		check_reg = (inst >> 3) & 127;
		if(check_reg == 3)
			strcat(names, "IOT 3");
		else if(check_reg == 4)
			strcat(names, "IOT 4");
		else
			strcat(names, "IOT <bad-device>");
	}

	if(opcode == 11) {
		check_reg = (inst >> 10) & 3;
		if(check_reg == 0)
			strcat(names, "ISZ");
		if(check_reg == 1)
			strcat(names, "JMP");
		if(check_reg == 2)
			strcat(names, "CALL");
	}

	if(opcode == 12) {
		check_reg = (inst >> 10) & 3;
		if(check_reg == 0) {
			strcat(names, "PUSH");
			if(overflow)
				strcat(names, " Stack Overflow");
		}
		if(check_reg == 1) {
			strcat(names, "POP");
			if(overflow)
				strcat(names, " Stack Underflow");
		}
	}

	if(opcode == 14) {
		check_reg = (inst >> 9) & 7;
		if(check_reg == 0)
			strcat(names, "MOD");
		if(check_reg == 1)
			strcat(names, "ADD");
		if(check_reg == 2)
			strcat(names, "SUB");
		if(check_reg == 3)
			strcat(names, "MUL");
		if(check_reg == 4)
			strcat(names, "DIV");
		if(check_reg == 5)
			strcat(names, "AND");

		if(check_reg == 6)
			strcat(names, "OR");
		if(check_reg == 7)
			strcat(names, "XOR");
	}

	if(opcode == 15) {
		//reg = addReg(inst);

		// SM
		check_reg = (inst >> 9) & 1;
		if(check_reg == 1) {
			strcat(names, "SM");
			addReg(inst, names);
			place = TRUE;
		}
		
		// SZ
		check_reg = (inst >> 8) & 1;
		if(check_reg == 1) {
			if(place)
				strcat(names, " ");
			else
				place = TRUE;
			strcat(names, "SZ");
			addReg(inst, names);
		}

		// SNL
		check_reg = (inst >> 7) & 1;
		if(check_reg == 1) {
			if(place)
				strcat(names, " ");
			else
				place = TRUE;
			strcat(names, "SNL");
		}

		// RSS
		check_reg = (inst >> 6) & 1;
		if(check_reg == 1) {
			if(place)
				strcat(names, " ");
			else
				place = TRUE;
			strcat(names, "RSS");
		}
	
		// CL
		check_reg = (inst >> 5) & 1;
		if(check_reg == 1) {
			if(place)
				strcat(names, " ");
			else
				place = TRUE;
			strcat(names, "CL");
			addReg(inst, names);
		}
	
		// CLL
		check_reg = (inst >> 4) & 1;
		if(check_reg == 1) {
			if(place)
				strcat(names, " ");
			else
				place = TRUE;
			strcat(names, "CLL");
		}

		// CM
		check_reg = (inst >> 3) & 1;
		if(check_reg == 1) {
			if(place)
				strcat(names, " ");
			else
				place = TRUE;
			strcat(names, "CM");
			addReg(inst, names);
		}

		// CML
		check_reg = (inst >> 2) & 1;
		if(check_reg == 1) {
			if(place)
				strcat(names, " ");
			else
				place = TRUE;
			strcat(names, "CML");
		}

		// DC
		check_reg = (inst >> 1) & 1;
		if(check_reg == 1) {
			if(place)
				strcat(names, " ");
			else
				place = TRUE;
			strcat(names, "DC");
			addReg(inst, names);
		}

		// In
		check_reg = inst & 1;
		if(check_reg == 1) {
			if(place)
				strcat(names, " ");
			else
				place = TRUE;
			strcat(names, "IN");
			addReg(inst, names);
		}
	}
	if((opcode >= 1 && opcode <= 9)) {
		addReg(inst, names);
	}

	
	// get size of opcode (names)
	size = strlen(names);
	
	// malloc size
	ret = malloc(size * sizeof(char) + 1);
	strcat(ret, names);
	return ret;
}
void verbose(unsigned short pc, unsigned short inst, int opcode) {
	fprintf(stderr, "Time %3lld: PC=0x%04X instruction = 0x%04X ", total_time, pc, inst);
	char *ops = get_opcode_names(opcode, inst);
	fprintf(stderr, "(%s)", ops);
	free(ops);
	if(inst != 0)
		get_regs(opcode, inst);
	fprintf(stderr, "\n");
}
void nonReg_nonMem(unsigned short old_pc) {
	if(debug)
		fprintf(stdout, "In nonReg_nonMem ");
	// NOP
	if(memory[old_pc] == 0) {
		if(debug)
			fprintf(stdout, " NOP\n");
	}
	// HLT
	if(memory[old_pc] == 1) {
		if(debug)
			fprintf(stdout, " HLT\n");
		value1 = regs[5];
		regs[5] = (regs[5] ^ 1);
	}
	
	// RET
	if(memory[old_pc] == 2) {
		if(debug)
			fprintf(stdout, " RET\n");
		value1 = regs[6];
		regs[6]++;
		regs[4] = memory[regs[6]];
		regs[6] = regs[6] & 65535;
		time++;
	}
	time++;
}
void execute(void) {
	int opcode = 0;
	unsigned short inst;
	unsigned short old_pc;
	regs[4] = ep & 65535;
	halt = FALSE;
	do {
		indirect = FALSE;
		time = 0;
		skip = FALSE;
		old_pc = regs[4];
		opcode = ((memory[old_pc] & opcode_mask) >> 12);
		inst = memory[old_pc];
		regs[4]++;
		if(debug)
			fprintf(stdout, "opcode = %i\n", opcode);
		// non-register, non-memory instructions
		if(opcode == 0)
			nonReg_nonMem(old_pc);	// Don't need to mess with pc
		// Register Memory Reference Instructions
		if(((opcode >= 1) && (opcode <= 9)) || opcode == 11 || opcode == 12)
			reg_mem_inst(opcode, old_pc);	// we can possibly mess with pc
		// I/O Transfer Instructions
		if(opcode == 10)
			do_io(opcode, old_pc);		// Don't need to mess with pc
		// Non-Register Memory Instructions
		if((opcode == 11) || (opcode == 12));
		// Register-to-Register Instructions
		if(opcode == 14)
			do_regToReg(old_pc);		// don't need to mess with pc
		// Non-memory Register Instructions
		if(opcode == 15)
			do_nonMemReg(old_pc);		// don't need to mess with pc

		// Begin ending instruction
		total_time = total_time + time;
		if(mode == 1)
			verbose(old_pc, inst, opcode);
		checkPSW();
		if(skip)
			regs[4]++;

		//halt=TRUE;
	} while (total_time <= 150000 && !halt);	//////////// get rid of total_time
}

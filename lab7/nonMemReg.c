#include "global.h"

// Deals with non-memory Register Instructions for the pdp429 program
Boolean checkBit(int distance, unsigned short old_pc) {
	int check_bit = (memory[old_pc] >> distance) & 1;
	if(check_bit == 1)
		return TRUE;
	else
		return FALSE;
}

Boolean checkReg(int distance, int reg) {
	int check_bit = (regs[reg] >> distance) & 1;
	if(check_bit == 1)
		return TRUE;
	else
		return FALSE;
}

void do_nonMemReg(unsigned short old_pc) {
	int reg = (memory[old_pc] >> 10) & 3;
	short temp;
	short temp2;
	reg1 = reg;
	Boolean check_bit;
	if(debug)
		fprintf(stdout, "In nonMemReg. Reg = %i value is %i", reg, regs[reg]);

	// check for SM (Skip if register is negative)
	check_bit = checkBit(9, old_pc);	// set value in reg to val1?
	value1 = regs[reg];
	if(check_bit) {
		if(debug)
			fprintf(stdout, " SM");
		check_bit = checkReg(15, reg);
		// if the number is negative
		if(check_bit)
			skip = TRUE;
	}
	
	// check for SZ
	check_bit = checkBit(8, old_pc);	// check instruction
	if(check_bit) {
		if(debug)
			fprintf(stdout, " SZ");
		
		if(regs[reg] == 0) {
			skip = TRUE;
		}
	}

	// check Link_Bit
	check_bit = checkBit(7, old_pc);
	if(check_bit) {
		if(debug)
			fprintf(stdout, " SNL");
		skipLink = link;
		if(link != 0)
			skip = TRUE;
	}

	// Reverse the Skip Sense
	check_bit = checkBit(6, old_pc);
	if(check_bit) {
		if(debug)
			fprintf(stdout, " RSS");
		// if skip == true
		if(skip)		
			skip = FALSE;
		// if skip == false
		else
			skip = TRUE;
	}
	
	// Clear register (CL)
	check_bit = checkBit(5, old_pc);
	if(check_bit) {
		if(debug)
			fprintf(stdout, " CL");
		regs[reg] = 0;
	}

	// Clear Link Bit (CLL)
	check_bit = checkBit(4, old_pc);
	if(check_bit) {
		if(debug)
			fprintf(stdout, " CLL");
		link = 0;
	}

	// Complement the Register (CM)
	check_bit = checkBit(3, old_pc);
	if(check_bit) {
		if(debug)
			fprintf(stdout, " CM");
		value2 = regs[reg];
		regs[reg] = ~regs[reg];
		regs[reg] = regs[reg] & 65535;
	}

	// Complement the Link Bit (CML)
	check_bit = checkBit(2, old_pc);
	if(check_bit) {
		if(debug)
			fprintf(stdout, " CML");
		compLink = link;
		link = ~link;
		link = link & 1;
	}

	// Decrement the Register by 1 (DC)
	check_bit = checkBit(1, old_pc);
	if(check_bit) {
		if(debug)
			fprintf(stdout, " DC");
		value3 = regs[reg];
		temp = regs[reg];
		regs[reg]--;
		temp2 = regs[reg];
		if(temp < 0 && temp2 > 0) {
			link = 1;
			overflow = TRUE;
		}
	}

	// Increment the Register by 1 (IN)
	check_bit = checkBit(0, old_pc);
	if(check_bit) {
		if(debug)
			fprintf(stdout, " IC");
		temp = regs[reg];
		regs[reg]++;
		temp2 = regs[reg];
		if(temp > 0 && temp2 < 0) {
			link = 1;
			overflow = TRUE;
		}

		if(temp < 0 && temp2 > 0) {
			link = 1;
			overflow = TRUE;
		}
	}
	if(debug)
		fprintf(stdout, " Reg %i is now 0x%04X\n", reg, regs[reg]);
	
	time++;

}

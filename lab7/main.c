#include "global.h"
#include "instant.h"



// scans argument looking for verbose
void scanargs(char *s) {
	s++;
	if(*s == 'v')
		mode = 1;
	else if(*s == 'd')
		debug = TRUE;
	else
		fprintf(stderr, "%c Flag not supported", *s);
}

// Main Driver of the program
int main(int argc, char *argv[]) {
	char *read;
	FILE *input;
	while(argc > 1) {
		argc--, argv++;
		read = *argv;
		if(*read == '-')
			scanargs(read);	
		else {
			input = fopen(read, "r");
			if(input == NULL) {
				printf("File did not open; exitting");
				return(-1);
			}
			// reads the object file
			readFile(input);
			execute();
		}
	}
	return 0;
}

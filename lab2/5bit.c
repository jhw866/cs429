#include <stdio.h>
#include <string.h>

/* Name: 	 Jeremy Wenzel 
 * EID: 	 jhw866
 * CSID: 	 jhw866
 * Unique ID:	 53865
 * Program Name: 5bit.c
 * Program description: Converts 8 bits and encodes them to 5 bits.
			It encodes between "A-Z" inclusive and "0-5" incluseive
*/

int decode = 0;	// determines wheather we decode or not

/* Helper function for encode */
/* Outputs the character to printf */
/* Characters are "A-Z" inclusive or "0-5" inclusive */
void displayNumber(short sum) {

	sum = (char) sum;
	
	// converts the sum to a letter, A-Z
	if(sum < 26)
		sum = sum + 65;
	// converts the sum to a number, 0-5
	else
		sum = sum + 22;
	printf("%c", sum);
}

/* Encoding function. This takes in a files and converts that file
   to a encoded file of 5 bits. */
void encode(FILE *file) {
	short bit;		// get bit
	short sum = 0;		// meant to hold the 5 bits
	int shifter = 4;	/* how many bits we have moved */
	int counter;		/* mean for for-loop */
	int newLine = 0;	/* if 0, don't have to add a newline to the encoded file */
        char c;
        c = getc(file);
	
	// while we are not at the end of the file
	while(!feof(file)){
		/* going to through the byte */
		for(counter = 8; counter > 0; counter--) {
			bit = (c & 128) >> 7; 		// get left most bit
			sum = sum + (bit << shifter);	// add that bit to 5 bit sum

			// this means we can now add this character to the encoded file
			if(shifter == 0) {
				// display the sum
				displayNumber(sum);

				// reset sum, shifter, and check newline
				sum = 0;
				shifter = 4;
				// if we have reached 72 characters in a line, print a newline
				if(newLine == 71) {
					printf("\n");
					newLine = 0;
				}

				else
					newLine++;
			}

			else
				shifter--;
			c = c << 1;
		}
		// get a new character
		c = fgetc(file);
	}
	// if we have moved from 4, we have to add that character to the file
	if(shifter != 4)
		displayNumber(sum);

	// if newLine != 0, then we need to add a new line character to the encoded file
	if(newLine != 0)
		printf("\n");

}

/* Decoding function. Takes in an encoded file and coverts it back to a 
   8 bit file. */
void decoder(FILE *file) {
	short bit;		// get bit
	short sum = 0;		// meant to hold the 8 bits
	int shifter = 7;	/* how many bits we have moved */
	int counter;		/* mean for for-loop */
	char display;
	int flag = 1;
	char c = getc(file);

	// while not at the end of the file
	while(!feof(file)) {

		/* if A-Z */
		if(c >= 65 && c <= 90) {
			c = c - 65;
			flag = 1;
		}
		/* if 0-5 */
		else if(c >= 48 && c <= 57){
			c = c - 22;
			flag = 1;
		}
		/* do not use this character, it is a newline */
		else
			flag = 0;
		
		c = c << 3; // move to get the 5th bit
		// go through character
		if(flag == 1) {
			/* go through the 5 bits */
			for(counter = 5; counter > 0; counter--) {
				bit = (c & 128) >> 7;		// get farthest left bit of c
				sum = sum + (bit << shifter);	// at that bit to the 8 bit sum

				// if we have reached a total 5 bit sum, display it
				if(shifter == 0) {		
					display = sum;
					printf("%c", display);
					sum = 0;
					shifter = 7;
				}
				
				else // keep adding the to them by moving shifter down 1
					shifter--;

				// read next bit
				c = c << 1;
			}
		}
		// read next character from file
		c = getc(file);
	}
	
}

/* Helper method of the main function */
/* scans the argument for usable flags */
void scanargs(char *s) {
	s++;
	switch(*s) {
		// if there is a "d", we decode
		case 'd':
			decode = 1;
			break;
		// all other cases
		default :
			printf("bad option %c\n", *s);
	}

}


/* Main driver of the program */
/* Gathers input from command line, and determines wheather to encode or decode */
/* After determining what to do, sends file to correct function */
int main(int argc, char *argv[]) {
	FILE *file;
	int namenotgiven = 1;

	// scan arguments for flags, but do not get files
	while (argc > 1)
	{
		argc--, argv++;
		// if there is a flag
		if (**argv == '-')
	                scanargs(*argv);

		// case that the file has been given
		else {
			namenotgiven = 0;
			file = fopen(*argv, "r");
			if (file == NULL) {
				printf("Can't open %s\n", *argv);
				return 1;
                       }
                }
        }
	
    /* input from stdin if not file named on the input line */
	if (namenotgiven == 1)
		file = stdin;
	// if we are encoding
	if (decode == 0)
		encode(file);
	else
		decoder(file);
	// if we opened a file, close it
	if (namenotgiven == 0)
		fclose(file);


	return 0;
}

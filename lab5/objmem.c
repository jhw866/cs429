/* 
   Assembler for PDP-8.  Memory and object file creation. 
*/

#include "asm8.h"


/* ***************************************************************** */
/*                                                                   */
/*                                                                   */
/* ***************************************************************** */

/* we want to assemble instructions.  We could assemble and output them
   all at once.  But we have a problem of forward references.  So we
   keep the (partially) assembled instructions in an array of them,
   essentially simulated memory.  That allows us to come back and 
   fix them up when the forward reference is resolved.

   We need to know which memory locations are from assembled
   instructions, and which are just empty; so each memory location
   has a bit (defined/not defined).
*/

INST     memory[4096];
Boolean defined[4096];
Address entry_point = 0;


void Clear_Object_Code(void)
{
    int i;
    for (i = 0; i < 4096; i++)
        {
            defined[i] = FALSE;
        }
}

void Define_Object_Code(Address addr, INST inst, Boolean redefine)
{
    if (debug)
        fprintf(stderr, "object code: 0x%03X = 0x%03X\n", addr, inst);
    if (defined[addr] && !redefine)
        {
            fprintf(stderr, "redefined memory location: 0x%03X: was 0x%03X; new value 0x%03X\n",
                    addr, memory[addr], inst);
            number_of_errors += 1;
        }
                
    defined[addr] = TRUE;
    memory[addr] = inst;
}

INST Fetch_Object_Code(Address addr)
{
    INST inst;

    if (defined[addr])
        inst = memory[addr];
    else
        inst = 0;

    if (debug)
        fprintf(stderr, "read object code: 0x%03X = 0x%03X\n", addr, inst);
    return(inst);
}


void Output_Object_Code(void)
{
    fprintf(output, "EP: %03X\n", entry_point);
    int i;
    for (i = 0; i < 4096; i++)
        {
            if (defined[i])
                fprintf(output, "%03X: %03X\n", i, memory[i]);
        }

}

void print_binary_code(short address, short size) {
	short i;

	size = (size * 2) + 3;
	putc(size, output);
	i = (address >> 6) & 63;
	putc(i, output);
	i = address & 63;
	putc(i, output);
	while(defined[address] != FALSE) {
		i = (memory[address] >> 6) & 63;
		putc(i, output);
		i = memory[address] & 63;
		putc(i, output);
		address++;
	}
}

void Output_Binary_Code(void) {
	short i;
	short address;
	short size = 0;
	Boolean defined_found = FALSE;
	putc('O', output);
	putc('B', output);
	putc('J', output);
	putc('8', output);
	i = (entry_point >> 6) & 63;
	putc(i, output);
	i = entry_point & 63;
	putc(i, output);
	for(i = 0; i < 4096; i++) {
		// if we have a defined value
		if(defined[i]) {
			// find the address
			if(defined_found == FALSE) {
				defined_found = TRUE;
				address = i;
			}
			// increment size
			size++;
		}
		
		else {
			// if we are at the end a defined block
			if(defined_found == TRUE) {
				print_binary_code(address, size);
				defined_found = FALSE;
				size = 0;
			}
		}
		if(size == 126) {
			// go to subroutine
			print_binary_code(address, size);
			defined_found = FALSE;
			size = 0;
		}
	}
}


